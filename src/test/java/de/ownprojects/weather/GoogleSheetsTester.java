package de.ownprojects.weather;

import static org.junit.jupiter.api.Assertions.*;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.TestPropertySource;

import com.google.api.services.sheets.v4.Sheets;
import com.google.api.services.sheets.v4.model.AppendValuesResponse;
import com.google.api.services.sheets.v4.model.BatchGetValuesResponse;
import com.google.api.services.sheets.v4.model.BatchUpdateSpreadsheetRequest;
import com.google.api.services.sheets.v4.model.DeleteDimensionRequest;
import com.google.api.services.sheets.v4.model.DimensionRange;
import com.google.api.services.sheets.v4.model.Request;
import com.google.api.services.sheets.v4.model.ValueRange;

import de.ownprojects.weather.googlesheets.SheetsServiceUtil;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(locations= "classpath:application-test.properties")
class GoogleSheetsTester {
	
	private static final ZoneId ZONE_ID = ZoneId.systemDefault();
	
	@Autowired
	TestRestTemplate restTemplate;
	
	@Autowired
	private Sheets sheetService;
	
	@Value("${SPREADSHEET_ID}")
	private String spreadsheetId;
	
	@Value("${WEATHER_DATA_SHEET_NAME}")
	private String weahterDataSheetName;
	
	private static List<WeatherEntry> mockWeather;
	
	private void clean() {
		//Delete test sheet content after each test
	    BatchUpdateSpreadsheetRequest content = new BatchUpdateSpreadsheetRequest();
	    Request request = new Request()
	    		  .setDeleteDimension(new DeleteDimensionRequest()
	    		    .setRange(new DimensionRange()
	    		      .setSheetId(0)
	    		      .setDimension("ROWS")
	    		      .setStartIndex(0)
	    		      .setEndIndex(1000)
	    		    )
	    		  );
	    List<Request> requests = new ArrayList<>();
	    requests.add(request);
	    content.setRequests(requests);

	    try {
	    	sheetService.spreadsheets().batchUpdate(this.spreadsheetId, content).execute();

	    } catch (IOException e) {
	        e.printStackTrace();
	    }
	}
	
	private void insert(List<WeatherEntry> insertEntries) {
		//Insert mock values		
		//Convert list of weather entries to list of list of strings that can be inserted as values of a value range
		List<List<Object>> transferEntries = insertEntries.stream().map(entry -> Arrays.asList(
				(Object) entry.getRoom(),
				(Object) entry.getTimestamp(),
				(Object) entry.getRelHumidity(),
				(Object) entry.getAbsHumidity(),
				(Object) entry.getTemperatureCelsius()))
				.collect(Collectors.toList());
		
		ValueRange appendBody = new ValueRange()
				  .setValues(transferEntries);
				
		AppendValuesResponse appendResult = null;
		try {
			appendResult = this.sheetService.spreadsheets().values()
			  .append(spreadsheetId, "A1", appendBody)
			  .setValueInputOption("USER_ENTERED")
			  .setInsertDataOption("INSERT_ROWS")
			  .setIncludeValuesInResponse(true)
			  .execute();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private <T> HttpEntity<T> requestEntityOf(T raw){
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		return new HttpEntity<T>(raw, headers);
	}
	
	@BeforeAll
	private static void startup() {
		mockWeather =  new ArrayList<>();
		String[] rooms = {"KitchenTest", "BathroomTest", "BalkonyTest"};

		for (String room : rooms){
			for(int i = 0; i < 3; i++){
				LocalDateTime datetime = LocalDateTime.now().minusDays(24 + i).plusMinutes(30);
				Long timestamp = datetime.atZone(ZONE_ID).toEpochSecond();
				WeatherEntry newEntry = new WeatherEntry(room, timestamp, 78.5, 12.3, 13.1);
				mockWeather.add(newEntry);
			}
		}
	}
	
	
	@BeforeEach
	private void setup() {
		this.clean();
		this.insert(mockWeather);
	}
		

	@Test
	void testSheetConnection() {
		Assertions.assertNotNull(this.sheetService);
	}
	
	@Test
	void readRange() {
   
	    List<String> ranges = Arrays.asList("A1","B1");
	    BatchGetValuesResponse readResult = null;
		try {
			readResult = this.sheetService.spreadsheets().values()
			  .batchGet(this.spreadsheetId)
			  .setRanges(ranges)
			  .execute();
		} catch (IOException e) {
			e.printStackTrace();
		}
	            
	    ValueRange value = readResult.getValueRanges().get(0);
	    
	    Assertions.assertNotNull(value);
	}
	
	@Test
	void readEntireSheet() {
   
	    ValueRange readResult = null;
		try {
			readResult = this.sheetService.spreadsheets().values()
			        .get(this.spreadsheetId, weahterDataSheetName)
			        .execute();
		} catch (IOException e) {
			e.printStackTrace();
		}
	    
	    Assertions.assertEquals(mockWeather.size(), readResult.getValues().size());
	}	
	
	
	@Test
	void writeTest() {
		
		int entriesPerRoom = 3;
		
		//Create list of weather entries
		List<WeatherEntry> mockWeather =  new ArrayList<>();
		String[] rooms = {"KitchenTest", "BathroomTest", "BalkonyTest"};
		
		int expectedRows = entriesPerRoom * rooms.length;

		for (String room : rooms){
			for(int i = 0; i < entriesPerRoom; i++){
				LocalDateTime datetime = LocalDateTime.now().minusDays(24 + i).plusMinutes(30);
				Long timestamp = datetime.atZone(ZONE_ID).toEpochSecond();
				WeatherEntry newEntry = new WeatherEntry(room, timestamp, 78.5, 12.3, 13.1);
				mockWeather.add(newEntry);
			}
		}
		
		//Convert list of weather entries to list of list of strings that can be inserted as values of a value range
		List<List<Object>> transferEntries = mockWeather.stream().map(entry -> Arrays.asList(
				(Object) entry.getRoom(),
				(Object) entry.getTimestamp(),
				(Object) entry.getRelHumidity(),
				(Object) entry.getAbsHumidity(),
				(Object) entry.getTemperatureCelsius()))
				.collect(Collectors.toList());
		
		ValueRange appendBody = new ValueRange()
				  .setValues(transferEntries);
				
		AppendValuesResponse appendResult = null;
		try {
			appendResult = this.sheetService.spreadsheets().values()
			  .append(spreadsheetId, "A1", appendBody)
			  .setValueInputOption("USER_ENTERED")
			  .setInsertDataOption("INSERT_ROWS")
			  .setIncludeValuesInResponse(true)
			  .execute();
		} catch (IOException e) {
			e.printStackTrace();
		}
		        
		ValueRange inserted = appendResult.getUpdates().getUpdatedData();
		
		Assertions.assertEquals(expectedRows, inserted.getValues().size());
	}
	
	@Test
	void getAllLast30DaysOk(){
		Long thirtyDaysAgo = Instant.now().getEpochSecond() - 30 * 24 * 60 * 60;
		int expectedQuantity = (int) mockWeather.stream()
				.filter(w -> w.getTimestamp() > thirtyDaysAgo)
				.count();

		ResponseEntity<List> response = restTemplate.getForEntity("/api/weather", List.class);
		Assertions.assertEquals(HttpStatus.OK, response.getStatusCode());
		Assertions.assertEquals(expectedQuantity, response.getBody().size());
	}
	
	@Test
	void getLast24HoursOk(){
		int expectedQuantity = 4;
		String testRoom = "BedRoomTest";
		LocalDateTime twentyFourHoursAgo = LocalDateTime.now().minusHours(24);
		Long twentyFourHoursAgoUnix = twentyFourHoursAgo.atZone(ZONE_ID).toEpochSecond();
		String url = "/api/weather?from=" + twentyFourHoursAgoUnix;
		
		List<WeatherEntry> entriesAround24HrsAgo = new ArrayList<>();
		for(int i = 0; i < 10; i++){
			LocalDateTime datetime = LocalDateTime.now().minusHours(i * 5).plusMinutes(30);
			Long timestamp = datetime.atZone(ZONE_ID).toEpochSecond();
			WeatherEntry newEntry = new WeatherEntry(testRoom,
					timestamp,
					78.5 + i,
					12.3 + i,
					13.1 + i);
			entriesAround24HrsAgo.add(newEntry);
		}
		
		this.clean();
		this.insert(entriesAround24HrsAgo);

		ResponseEntity<List> response = restTemplate.getForEntity(url, List.class);
		Assertions.assertEquals(HttpStatus.OK, response.getStatusCode());
		Assertions.assertEquals(expectedQuantity, response.getBody().size());
	}

	@Test
	void postAndGetLastTwoHoursOk(){
		
		//Sleep first, so googles rate limit is not exceeded 
		try {
			Thread.sleep(100000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		int expectedQuantity = 24;
		String testRoom = "BedRoomTest";
		LocalDateTime twoHoursAgo = LocalDateTime.now().minusHours(2);
		Long twoHoursAgoUnix = twoHoursAgo.atZone(ZONE_ID).toEpochSecond();
		String url = "/api/weather?from=" + twoHoursAgoUnix;

		this.clean();

		for(int i = -12; i < 36; i++){
			LocalDateTime datetime = LocalDateTime.now().minusMinutes(i * 5).plusMinutes(3);
			Long timestamp = datetime.atZone(ZONE_ID).toEpochSecond();
			WeatherEntry newEntry = new WeatherEntry(testRoom,
					timestamp,
					78.5 + i,
					12.3 + i,
					13.1 + i);

			ResponseEntity<WeatherEntry> postResponse = restTemplate.postForEntity("/api/weather",
					this.requestEntityOf(newEntry),
					WeatherEntry.class);

			Assertions.assertEquals(HttpStatus.OK, postResponse.getStatusCode());
		}

		ResponseEntity<List> response = restTemplate.getForEntity(url, List.class);

		Assertions.assertEquals(HttpStatus.OK, response.getStatusCode());
		Assertions.assertEquals(expectedQuantity, response.getBody().size());

	}
	
	@Test
	void postNewEntry(){
		Double testRelHumidity = 66.8;
		int testTotalNumberOfEntries = mockWeather.size() + 1;
		WeatherEntry newWeatherEntry = new WeatherEntry("postTest", Instant.now().getEpochSecond(), testRelHumidity, 45.6, 32.1);

		ResponseEntity<WeatherEntry> response = restTemplate.postForEntity("/api/weather",
				this.requestEntityOf(newWeatherEntry),
				WeatherEntry.class);

		Assertions.assertEquals(HttpStatus.OK, response.getStatusCode());
		Assertions.assertNotNull(response.getBody());
		Assertions.assertEquals(testRelHumidity, response.getBody().getRelHumidity());

		Assertions.assertEquals(testTotalNumberOfEntries, mockWeather.size() + 1);

	}
	
	@Test
	void getLatestEntries(){
		
		//Sleep first, so googles rate limit is not exceeded 
		try {
			Thread.sleep(100000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		Long latestTimestamp = Instant.now().getEpochSecond();
		String[] testRooms = {"BalkonyTest", "KitchenTest"};
		Double temperature = 5.2;
		
		//Save some new entries
		for(String room : testRooms){
			WeatherEntry newEntry = new WeatherEntry(room, latestTimestamp, 78.2, 12.2, temperature);

			ResponseEntity<WeatherEntry> postResponse = restTemplate.postForEntity("/api/weather",
					this.requestEntityOf(newEntry),
					WeatherEntry.class);

			Assertions.assertEquals(HttpStatus.OK, postResponse.getStatusCode());
		}
		
		//Get latest entries
		ResponseEntity<CurrentWeatherEntry[]> responseLatest = restTemplate.getForEntity("/api/weather/latest", CurrentWeatherEntry[].class);
		
		Assertions.assertEquals(HttpStatus.OK, responseLatest.getStatusCode());
		Assertions.assertNotNull(responseLatest.getBody());
		
		List<CurrentWeatherEntry> returnedEntries = Arrays.asList(responseLatest.getBody());
		
		//Every room only appears once
		int distinctCount = (int) returnedEntries.stream().map(c -> c.getRoom()).distinct().count();		
		Assertions.assertEquals(distinctCount, responseLatest.getBody().length);
		
		//Two saved entries are among the returned ones
		for (String room : testRooms) {
			Optional<CurrentWeatherEntry> latestEntryOfRoomOpt = returnedEntries.stream().
					filter(c -> c.getRoom().equals(room)).
					findAny();
			
			CurrentWeatherEntry latestEntryOfRoom = Assertions.assertDoesNotThrow(() -> latestEntryOfRoomOpt.get());
			Assertions.assertEquals(latestTimestamp, latestEntryOfRoom.getTimestamp());
			Assertions.assertEquals(temperature, latestEntryOfRoom.getTemperatureCelsius());
		}

	}
	
}

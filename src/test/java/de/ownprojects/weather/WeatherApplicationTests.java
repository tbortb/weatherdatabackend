package de.ownprojects.weather;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.*;
import org.springframework.test.context.TestPropertySource;
import org.springframework.transaction.annotation.Transactional;

import com.google.api.services.sheets.v4.Sheets;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Stream;


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(locations= "classpath:application-test.properties")
@Disabled("This is a test in case a database is used as a repository. Currently a google sheet is used!")
class WeatherApplicationTests {

	@Autowired
	TestRestTemplate restTemplate;

	@Autowired
	WeatherRepository weatherRepository;

	@Autowired
	LatestWeatherRepository latestRepository;

	List<WeatherEntry> mockWeather;

	private static final ZoneId ZONE_ID = ZoneId.systemDefault();

	private <T> HttpEntity<T> requestEntityOf(T raw){
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		return new HttpEntity<T>(raw, headers);
	}

	@BeforeEach
	void setUp(){

		mockWeather = new ArrayList<>();
		String[] rooms = {"KitchenTest", "BathroomTest", "BalkonyTest"};

		for (String room : rooms){
			for(int i = 0; i < 10; i++){
				LocalDateTime datetime = LocalDateTime.now().minusDays(24 + i).plusMinutes(30);
				Long timestamp = datetime.atZone(ZONE_ID).toEpochSecond();
				WeatherEntry newEntry = new WeatherEntry(room, timestamp, 78.5, 12.3, 13.1);
				mockWeather.add(newEntry);
			}
		}
		weatherRepository.saveAll(mockWeather);
	}

	@AfterEach
	void clean(){
		weatherRepository.deleteAll();
		latestRepository.deleteAll();
	}

	@Disabled("This is a test in case a database is used as a repository. Currently a google sheet is used!")
	@Test
	void contextLoads() {
	}

	@Disabled("This is a test in case a database is used as a repository. Currently a google sheet is used!")
	@Test
	void getAllLast30DaysOk(){
		Long thirtyDaysAgo = Instant.now().getEpochSecond() - 30 * 24 * 60 * 60;
		int expectedQuantity = (int) mockWeather.stream()
				.filter(w -> w.getTimestamp() > thirtyDaysAgo)
				.count();

		ResponseEntity<List> response = restTemplate.getForEntity("/api/weather", List.class);
		Assertions.assertEquals(HttpStatus.OK, response.getStatusCode());
		Assertions.assertEquals(expectedQuantity, response.getBody().size());
	}

	@Disabled("This is a test in case a database is used as a repository. Currently a google sheet is used!")
	@Test
	void getLast24HoursOk(){
		int expectedQuantity = 4;
		String testRoom = "BedRoomTest";
		LocalDateTime twentyFourHoursAgo = LocalDateTime.now().minusHours(24);
		Long twentyFourHoursAgoUnix = twentyFourHoursAgo.atZone(ZONE_ID).toEpochSecond();
		String url = "/api/weather?from=" + twentyFourHoursAgoUnix;
		
		List<WeatherEntry> entriesAround24HrsAgo = new ArrayList<>();
		for(int i = 0; i < 10; i++){
			LocalDateTime datetime = LocalDateTime.now().minusHours(i * 5).plusMinutes(30);
			Long timestamp = datetime.atZone(ZONE_ID).toEpochSecond();
			WeatherEntry newEntry = new WeatherEntry(testRoom,
					timestamp,
					78.5 + i,
					12.3 + i,
					13.1 + i);
			entriesAround24HrsAgo.add(newEntry);
		}
		weatherRepository.deleteAll();
		weatherRepository.saveAll(entriesAround24HrsAgo);

		ResponseEntity<List> response = restTemplate.getForEntity(url, List.class);
		Assertions.assertEquals(HttpStatus.OK, response.getStatusCode());
		Assertions.assertEquals(expectedQuantity, response.getBody().size());
	}

	@Disabled("This is a test in case a database is used as a repository. Currently a google sheet is used!")
	@Test
	void getLastTwoHoursOk(){
		int expectedQuantity = 24;
		String testRoom = "BedRoomTest";
		LocalDateTime twoHoursAgo = LocalDateTime.now().minusHours(2);
		Long twoHoursAgoUnix = twoHoursAgo.atZone(ZONE_ID).toEpochSecond();
		String url = "/api/weather?from=" + twoHoursAgoUnix;

		weatherRepository.deleteAll();

		for(int i = -12; i < 36; i++){
			LocalDateTime datetime = LocalDateTime.now().minusMinutes(i * 5).plusMinutes(3);
			Long timestamp = datetime.atZone(ZONE_ID).toEpochSecond();
			WeatherEntry newEntry = new WeatherEntry(testRoom,
					timestamp,
					78.5 + i,
					12.3 + i,
					13.1 + i);

			ResponseEntity<WeatherEntry> postResponse = restTemplate.postForEntity("/api/weather",
					this.requestEntityOf(newEntry),
					WeatherEntry.class);

			Assertions.assertEquals(HttpStatus.OK, postResponse.getStatusCode());
		}

		ResponseEntity<List> response = restTemplate.getForEntity(url, List.class);

		Assertions.assertEquals(HttpStatus.OK, response.getStatusCode());
		Assertions.assertEquals(expectedQuantity, response.getBody().size());

	}

	@Disabled("This is a test in case a database is used as a repository. Currently a google sheet is used!")
	@Test
	void postNewEntry(){
		Double testRelHumidity = 66.8;
		int testTotalNumberOfEntries = mockWeather.size() + 1;
		WeatherEntry newWeatherEntry = new WeatherEntry("postTest", Instant.now().getEpochSecond(), testRelHumidity, 45.6, 32.1);

		ResponseEntity<WeatherEntry> response = restTemplate.postForEntity("/api/weather",
				this.requestEntityOf(newWeatherEntry),
				WeatherEntry.class);

		Assertions.assertEquals(HttpStatus.OK, response.getStatusCode());
		Assertions.assertNotNull(response.getBody());
		Assertions.assertEquals(testRelHumidity, response.getBody().getRelHumidity());

		Assertions.assertEquals(testTotalNumberOfEntries, weatherRepository.findAll().size());
		Assertions.assertEquals(1, weatherRepository.findAll()
				.stream()
				.filter(w -> w.getRoom().equals("postTest"))
				.count());

	}

	@Disabled("This is a test in case a database is used as a repository. Currently a google sheet is used!")
	@Test
	void getLatestEntries(){
		latestRepository.save(new CurrentWeatherEntry("BalkonyTest", Instant.now().getEpochSecond(), 78.2, 12.2, 78.2));
		latestRepository.save(new CurrentWeatherEntry("KitchenTest", Instant.now().getEpochSecond(), 78.2, 12.2, 78.2));

		ResponseEntity<CurrentWeatherEntry[]> responseLatest = restTemplate.getForEntity("/api/weather/latest", CurrentWeatherEntry[].class);

		Assertions.assertEquals(HttpStatus.OK, responseLatest.getStatusCode());
		Assertions.assertNotNull(responseLatest.getBody());
		Assertions.assertEquals(2, responseLatest.getBody().length);
		Assertions.assertNotNull(responseLatest.getBody()[0].getRoom());
	}

	@Disabled("This is a test in case a database is used as a repository. Currently a google sheet is used!")
	@Test
	void addEntriesUpdatesLatestEnties(){
		WeatherEntry balkonyTest1 = new WeatherEntry("LatestTest1", Instant.now().getEpochSecond(), 100.0, 12.2, 78.2);
		WeatherEntry kitchenTest1 = new WeatherEntry("LatestTest2", Instant.now().getEpochSecond(), 78.2, 12.2, 78.2);
		WeatherEntry balkonyTest2 = new WeatherEntry("LatestTest1", Instant.now().getEpochSecond(), 78.2, 12.2, 78.2);

		WeatherEntry[] newEntries = {balkonyTest1, kitchenTest1, balkonyTest2};
		int expectedLatest = (int) Stream.of(newEntries).map(e -> e.getRoom()).distinct().count();
		int expectedTotalEntries = mockWeather.size() + newEntries.length;

		Long currentDate = Instant.now().getEpochSecond();
		Long thirtyDayAgo = currentDate - 30 * 24 * 60 * 60;

		int expectedLast30Days = this.weatherRepository.findByTimestampBetween(thirtyDayAgo,
				currentDate).size() +
				newEntries.length;

		for (WeatherEntry postEntry : newEntries){
			ResponseEntity<WeatherEntry> response = restTemplate.postForEntity("/api/weather",
				this.requestEntityOf(postEntry),
				WeatherEntry.class);

			try {
				Thread.sleep(400);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

		}

		ResponseEntity<CurrentWeatherEntry[]> responseLatest = restTemplate.getForEntity("/api/weather/latest",
				CurrentWeatherEntry[].class);
		ResponseEntity<WeatherEntry[]> responseAll = restTemplate.getForEntity("/api/weather",
				WeatherEntry[].class);

		Assertions.assertEquals(HttpStatus.OK, responseLatest.getStatusCode());
		Assertions.assertNotNull(responseLatest.getBody());
		Assertions.assertEquals(expectedLatest, responseLatest.getBody().length);
		Assertions.assertTrue(
				Stream.of(responseLatest.getBody())
				.noneMatch(w ->  w.getRelHumidity() > 80)
		);
		Assertions.assertEquals(expectedLatest, latestRepository.findAll().size());
		Assertions.assertEquals(expectedTotalEntries, weatherRepository.findAll().size());
		Assertions.assertEquals(expectedLast30Days, responseAll.getBody().length);

	}

	
}

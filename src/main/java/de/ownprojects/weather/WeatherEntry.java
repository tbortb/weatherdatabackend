package de.ownprojects.weather;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;

@Entity
public class WeatherEntry implements Comparable<WeatherEntry> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long id;
    private String room;
    private Long timestamp;
    private Double relHumidity;
    private Double absHumidity;
    private Double temperatureCelsius;

    public WeatherEntry() {}

    public WeatherEntry(String room, Long timestamp, Double relHumidity, Double absHumidity, Double temperatureCelsius) {
        this.room = room;
        this.timestamp = timestamp;
        this.relHumidity = relHumidity;
        this.absHumidity = absHumidity;
        this.temperatureCelsius = temperatureCelsius;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRoom() {
        return this.room;
    }

    public void setRoom(String room) {
        this.room = room;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }

    public Double getRelHumidity() {
        return relHumidity;
    }

    public void setRelHumidity(Double relHumidity) {
        this.relHumidity = relHumidity;
    }

    public Double getAbsHumidity() {
        return absHumidity;
    }

    public void setAbsHumidity(Double absHumidity) {
        this.absHumidity = absHumidity;
    }

    public Double getTemperatureCelsius() {
        return temperatureCelsius;
    }

    public void setTemperatureCelsius(Double temperatureCelsius) {
        this.temperatureCelsius = temperatureCelsius;
    }

    public CurrentWeatherEntry createCurrentEntry() {
        return new CurrentWeatherEntry(this.room, this.timestamp, this.relHumidity, this.absHumidity, this.temperatureCelsius);
    }

	@Override
	public int compareTo(WeatherEntry other) {
		return (int) (this.timestamp - other.getTimestamp());
	}
}

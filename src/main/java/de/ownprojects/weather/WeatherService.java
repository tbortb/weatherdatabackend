package de.ownprojects.weather;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

@Service
public class WeatherService {

    @Autowired
    WeatherRepository weatherRepository;

    @Autowired
    LatestWeatherRepository latestRepository;

    public List<WeatherEntry> getLast30Days(){
        Long currentDate = Instant.now().getEpochSecond();
        Long thirtyDayAgo = currentDate - 30 * 24 * 60 * 60;
        return weatherRepository.findByTimestampBetween(thirtyDayAgo, currentDate);
    }

    public List<WeatherEntry> getWeatherDataBetween(Long from, Long to){
        if(from == null){
            throw new RuntimeException("Parameter [from] must not be null");
        }
        if(to == null){
            to = Instant.now().getEpochSecond();
        }
        return weatherRepository.findByTimestampBetween(from, to);
    }

    public WeatherEntry addWeatherEntry(WeatherEntry postWeatherEntry){
        WeatherEntry newEntry = weatherRepository.save(postWeatherEntry);
        latestRepository.save(newEntry.createCurrentEntry());
        return newEntry;
    }

    public List<CurrentWeatherEntry> getLatestWeatherEntries(){
        return this.latestRepository.findAll();
    }

}

package de.ownprojects.weather;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;

@Entity
public class CurrentWeatherEntry {

    @Id
    private String room;
    //@JsonFormat(pattern = "dd.MM.yyyy HH:mm:ss")
    private Long timestamp;
    private Double relHumidity;
    private Double absHumidity;
    private Double temperatureCelsius;

    public CurrentWeatherEntry() {}

    public CurrentWeatherEntry(String room, Long timestamp, Double relHumidity, Double absHumidity, Double temperatureCelsius) {
        this.room = room;
        this.timestamp = timestamp;
        this.relHumidity = relHumidity;
        this.absHumidity = absHumidity;
        this.temperatureCelsius = temperatureCelsius;
    }

    public String getRoom() {
        return room;
    }

    public void setRoom(String room) {
        this.room = room;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }

    public Double getRelHumidity() {
        return relHumidity;
    }

    public void setRelHumidity(Double relHumidity) {
        this.relHumidity = relHumidity;
    }

    public Double getAbsHumidity() {
        return absHumidity;
    }

    public void setAbsHumidity(Double absHumidity) {
        this.absHumidity = absHumidity;
    }

    public Double getTemperatureCelsius() {
        return temperatureCelsius;
    }

    public void setTemperatureCelsius(Double temperatureCelsius) {
        this.temperatureCelsius = temperatureCelsius;
    }
}

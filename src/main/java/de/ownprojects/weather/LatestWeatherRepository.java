package de.ownprojects.weather;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface LatestWeatherRepository extends JpaRepository<CurrentWeatherEntry, Long> {
    //void deleteByRoom(String room);
    //CurrentWeatherEntry findByRoom(String room);
}

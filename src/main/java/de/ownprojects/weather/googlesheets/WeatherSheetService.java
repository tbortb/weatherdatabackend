package de.ownprojects.weather.googlesheets;

import java.time.Instant;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.api.services.sheets.v4.Sheets;

import de.ownprojects.weather.CurrentWeatherEntry;
import de.ownprojects.weather.LatestWeatherRepository;
import de.ownprojects.weather.WeatherEntry;
import de.ownprojects.weather.WeatherRepository;

@Service
public class WeatherSheetService {

    @Autowired
    WeatherSheetRepo sheetRepo;

    public List<WeatherEntry> getLast30Days(){
        Long currentDate = Instant.now().getEpochSecond();
        Long thirtyDayAgo = currentDate - 30 * 24 * 60 * 60;
        return sheetRepo.findByTimestampBetween(thirtyDayAgo, currentDate);
    }

    public List<WeatherEntry> getWeatherDataBetween(Long from, Long to){
        if(from == null){
            throw new RuntimeException("Parameter [from] must not be null");
        }
        if(to == null){
            to = Instant.now().getEpochSecond();
        }
        return sheetRepo.findByTimestampBetween(from, to);
    }

    public WeatherEntry addWeatherEntry(WeatherEntry postWeatherEntry){
        WeatherEntry newEntry = sheetRepo.addWeatherEntry(postWeatherEntry);
        return newEntry;
    }

    public List<CurrentWeatherEntry> getLatestWeatherEntries(){
        return this.sheetRepo.getLatest();
    }

}

package de.ownprojects.weather.googlesheets;

import java.io.IOException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.google.api.services.sheets.v4.Sheets;
import com.google.api.services.sheets.v4.model.AppendValuesResponse;
import com.google.api.services.sheets.v4.model.ValueRange;

import de.ownprojects.weather.CurrentWeatherEntry;
import de.ownprojects.weather.WeatherEntry;

@Service
public class WeatherSheetRepo {
	
	@Value("${WEATHER_DATA_SHEET_NAME}")
	private String weahterDataSheetName;
	
	@Value("${SPREADSHEET_ID}")
	private String spreadsheetId;
	
	@Autowired
	private Sheets sheetService;
	
	public List<WeatherEntry> findByTimestampBetween(Long from, Long to){
		List<WeatherEntry> weatherEntries =  this.getAllWeatherEntries();
		
		List<WeatherEntry> weatherEntriesInRange = weatherEntries.
				stream().
				filter(w -> w.getTimestamp() >= from && w.getTimestamp() < to).
				collect(Collectors.toList());
	    
        return weatherEntriesInRange;
    }

    public WeatherEntry addWeatherEntry(WeatherEntry postWeatherEntry){
    	
    	List<List<Object>> toSaveEntry = Arrays.asList(Arrays.asList(postWeatherEntry.getRoom(),
    			postWeatherEntry.getTimestamp(), postWeatherEntry.getRelHumidity(),
    			postWeatherEntry.getAbsHumidity(), postWeatherEntry.getTemperatureCelsius())); 
    	
    	
    	ValueRange appendBody = new ValueRange()
				  .setValues(toSaveEntry);
				
		AppendValuesResponse appendResult = null;
		try {
			appendResult = this.sheetService.spreadsheets().values()
			  .append(this.spreadsheetId, "A1", appendBody)
			  .setValueInputOption("USER_ENTERED")
			  .setInsertDataOption("INSERT_ROWS")
			  .setIncludeValuesInResponse(true)
			  .execute();
		} catch (IOException e) {
			e.printStackTrace();
		}
		        
		ValueRange inserted = appendResult.getUpdates().getUpdatedData();
   	
        return this.valueRangeToWeatherEntries(inserted).get(0);
    }

    public List<CurrentWeatherEntry> getLatest(){
    	List<WeatherEntry> weatherEntries =  this.getAllWeatherEntries();    	
    	
    	Map<String, WeatherEntry> latest = weatherEntries.stream()
    			   .collect(Collectors.toMap(WeatherEntry::getRoom, Function.identity(),
    			   BinaryOperator.maxBy(Comparator.comparing(WeatherEntry::getTimestamp))));    	
    	
        return latest.values().stream().map(e -> e.createCurrentEntry()).collect(Collectors.toList());
    }
    
    private List<WeatherEntry> getAllWeatherEntries(){
    	//Get all values from google sheet
    	ValueRange readResult = null;
		try {
			readResult = this.sheetService.spreadsheets().values()
			        .get(this.spreadsheetId, weahterDataSheetName)
			        .execute();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return valueRangeToWeatherEntries(readResult);
    }
    
    private List<WeatherEntry> valueRangeToWeatherEntries(ValueRange vr){
    	List<WeatherEntry> weatherEntries = new ArrayList<>();
    	for (List<Object> entryData : vr.getValues()){
			WeatherEntry weatherEntry = new WeatherEntry((String) entryData.get(0),
														Long.valueOf((String) entryData.get(1)),
														Double.valueOf((String) entryData.get(2)),
														Double.valueOf((String) entryData.get(3)),
														Double.valueOf((String) entryData.get(4)));
			weatherEntries.add(weatherEntry);
		}
    	
		return weatherEntries;
    }

}

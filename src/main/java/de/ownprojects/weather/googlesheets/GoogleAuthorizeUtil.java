package de.ownprojects.weather.googlesheets;

import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.security.GeneralSecurityException;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential.Builder;
import com.google.api.client.googleapis.util.Utils;
import com.google.api.services.sheets.v4.SheetsScopes;

@SuppressWarnings("deprecation")
@Service
public class GoogleAuthorizeUtil {

	
	
    private String type;
    private String projectId;
    private String privateKeyId;
    private String privateKey;
    private String clientEmail;
    private String tokenUri;
    private String clientId;    
    
    public GoogleAuthorizeUtil(@Value("${type}") String type,
    		@Value("${project_id}") String projectId,
    		@Value("${private_key_id}") String privateKeyId,
    		@Value("${private_key}") String privateKey,
    		@Value("${client_email}") String clientEmail, 
    		@Value("${token_uri}") String tokenUri, 
    		@Value("${client_id}") String clientId) {
		super();
		this.type = type;
		this.projectId = projectId;
		this.privateKeyId = privateKeyId;
		this.privateKey = privateKey;
		this.clientEmail = clientEmail;
		this.tokenUri = tokenUri;
		this.clientId = clientId;
	}



	public GoogleCredential authorize() throws IOException, GeneralSecurityException {
//        InputStream inStream = new ClassPathResource("weather_google_sheet_client_secret.json").getInputStream();
        
        //Needed default info transport is Utils.getDefaultTransport(), jsonFactory is Utils.getDefaultJsonFactory(), emptyScopes are set by calling .createScoped(scopes)
        
        //Needed info from stream:
//        clientEmail, privateKey, privateKeyId, token_uri, project_id
        
        
        String credentialString = "{ \"type\": \"" + this.type + 
        		"\", \"project_id\": \"" + this.projectId + 
        		"\", \"private_key_id\": \"" + this.privateKeyId +
        		"\", \"private_key\": \"" + this.privateKey +
        		"\", \"client_email\": \"" + this.clientEmail +
        		"\", \"client_id\": \"" + this.clientId +
        		"\", \"token_uri\": \"" + this.tokenUri + 
        		"\"}";
        
        InputStream in = new ByteArrayInputStream(credentialString.getBytes());
        
        List<String> scopes = Arrays.asList(SheetsScopes.SPREADSHEETS);

        //Deprecation warning seems incorrect https://stackoverflow.com/questions/64135720/how-do-you-access-a-google-sheet-with-a-service-account-from-java
        GoogleCredential sourceCredentials = GoogleCredential.fromStream(in).createScoped(scopes);
        return sourceCredentials;
    }

}

package de.ownprojects.weather.googlesheets;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.json.gson.GsonFactory;
import com.google.api.services.sheets.v4.Sheets;

import java.io.IOException;
import java.security.GeneralSecurityException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@SuppressWarnings("deprecation")
public class SheetsServiceUtil {

    private static final String APPLICATION_NAME = "WeatherDataClient";
    
    @Autowired
    GoogleAuthorizeUtil authenticator;

    @Bean
    public Sheets getSheetsService() throws IOException, GeneralSecurityException {
    	GoogleCredential credential = authenticator.authorize();
        return new Sheets.
        		Builder(GoogleNetHttpTransport.newTrustedTransport(),
        				GsonFactory.getDefaultInstance(),
        				credential).
        		setApplicationName(APPLICATION_NAME).
        		build();
    }

}
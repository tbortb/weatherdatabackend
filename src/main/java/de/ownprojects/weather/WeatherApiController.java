package de.ownprojects.weather;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import de.ownprojects.weather.googlesheets.WeatherSheetService;

import java.time.LocalDateTime;
import java.util.List;

@Controller
@RequestMapping("/api/weather")
@CrossOrigin(origins = {"http://letshopeitswarm.herokuapp.com", "http://localhost:3000"})
public class WeatherApiController {

    @Autowired
    private WeatherSheetService weatherService;

    @GetMapping
    public ResponseEntity<List<WeatherEntry>> getWeatherData(@RequestParam(required = false)
                                                                 Long from,
                                                             @RequestParam(required = false)
                                                             Long to){

        if (from == null && to == null){
            return ResponseEntity.ok(this.weatherService.getLast30Days());
        }
        return ResponseEntity.ok(this.weatherService.getWeatherDataBetween(from, to));
    }

    @PostMapping
    public ResponseEntity<WeatherEntry> addWeatherEntry(@RequestBody WeatherEntry newEntry){
        return ResponseEntity.ok(this.weatherService.addWeatherEntry(newEntry));
    }

    @GetMapping("/latest")
    public ResponseEntity<List<CurrentWeatherEntry>> getLatestWeatherEntries(){
        return ResponseEntity.ok(this.weatherService.getLatestWeatherEntries());
    }

    @ExceptionHandler
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public void invalidArgumentException(IllegalArgumentException e) {}

}

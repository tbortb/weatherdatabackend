package de.ownprojects.weather;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

@Repository
public interface WeatherRepository extends JpaRepository<WeatherEntry, Long> {
    List<WeatherEntry> findByTimestampBetween(Long newBound, Long oldBound);
}
